'use strict'

import GLib from 'gi://GLib';
import Adw from 'gi://Adw';
import GObject from 'gi://GObject';
import Gio from 'gi://Gio';
import Gtk from 'gi://Gtk';
import Gdk from 'gi://Gdk';

export const About = GObject.registerClass({
    GTypeName: 'AboutPrefs',
    Template: GLib.Uri.resolve_relative(import.meta.url, '../ui/about.ui',GLib.UriFlags.NONE),
    InternalChildren: [
        'app_icon_image',
        'app_name_label',
        'version_label',
    ]
}, class About extends Adw.PreferencesPage {
    constructor(window) {
        super({});

        this._app_icon_image.icon_name = 'drive-harddisk-symbolic';
        this._app_name_label.label = window._metadata.name;
        this._version_label.label = window._metadata.version ? window._metadata.version.toString() : "0";

        // setup menu actions
        const actionGroup = new Gio.SimpleActionGroup();
        window.insert_action_group('prefs', actionGroup);

        // a list of actions with their associated link
        const actions = [
            {
                name: 'open-bug-report',
                link: 'https://gitlab.com/marcosdalvarez/drive-activity-indicator-extension/-/issues'
            },
            {
                name: 'open-readme',
                link: 'https://gitlab.com/marcosdalvarez/drive-activity-indicator-extension/'
            },
            {
                name: 'open-license',
                link: 'https://gitlab.com/marcosdalvarez/drive-activity-indicator-extension/-/blob/main/LICENSE'
            },
        ];

        actions.forEach(action => {
            let act = new Gio.SimpleAction({ name: action.name });
            act.connect(
                'activate',
                _ => Gtk.show_uri(window, action.link, Gdk.CURRENT_TIME)
            );
            actionGroup.add_action(act);
        });
    }
});