'use strict'

import GLib from 'gi://GLib';
import Adw from 'gi://Adw';
import GObject from 'gi://GObject';
import Gio from 'gi://Gio';

/**
 * Bind Gtk.ColorDialogButton to settings
 * 
 * @param {Gtk.ColorDialogButton} button 
 * @param {Gio.Settings} settings 
 * @param {String} key 
 */
function bindColorDialogButton(button, settings, key) {
    const tmpColor = button.rgba;
    tmpColor.parse(settings.get_string(key));
    button.rgba = tmpColor;
    button.connect('notify::rgba', () => {
        settings.set_string(key, button.rgba.to_string());
    });
    settings.connect(`changed::${key}`, () => {
        tmpColor.parse(settings.get_string(key));
        button.rgba = tmpColor;
    });
}

export const General = GObject.registerClass({
    GTypeName: 'GeneralPrefs',
    Template: GLib.Uri.resolve_relative(import.meta.url, '../ui/general.ui',GLib.UriFlags.NONE),
    InternalChildren: [
        'include_removables',
        'update_interval',
        'reset_all',
        'reset_dialog',
        'idle-color-button',
        'read-color-button',
        'write-color-button',
        'read-write-color-button',        
    ],
}, class General extends Adw.PreferencesPage {
    constructor(window) {
        super({});

        window._settings.bind(
            'include-removable-drives', 
            this._include_removables, 
            'active', 
            Gio.SettingsBindFlags.DEFAULT
        );
        window._settings.bind(
            'update-interval', 
            this._update_interval, 
            'value', 
            Gio.SettingsBindFlags.DEFAULT
        );

        bindColorDialogButton(this._idle_color_button, window._settings, 'idle-color');
        bindColorDialogButton(this._read_color_button, window._settings, 'read-color');
        bindColorDialogButton(this._write_color_button, window._settings, 'write-color');
        bindColorDialogButton(this._read_write_color_button, window._settings, 'read-write-color');

        this._reset_dialog.connect('response', (obj, response, data) => {
            if (response === 'reset') {
                this._resetSettings(window._settings);
                window._driver.enableAll();
            }
        });

        this._reset_all.connect('clicked', () => {
            this._reset_dialog.transientFor = this.root;
            this._reset_dialog.present();
        });
    }

    /**
     * Reset all (recursively) settings values to default
     * 
     * @param {Gio.Settings} settings Settings to reset
     */
    _resetSettings(settings) {
        const keys = settings.settings_schema.list_keys();
        keys.forEach(key => {
            settings.reset(key);
        });

        const childrens = settings.settings_schema.list_children();
        childrens.forEach(children => {
            const childrenSettings = settings.get_child(children);
            this._resetSettings(childrenSettings);
        });
    }
});