'use strict';

import GObject from 'gi://GObject';
import GLib from 'gi://GLib';
import Gio from 'gi://Gio';

import * as Main from 'resource:///org/gnome/shell/ui/main.js';
import * as QuickSettings from 'resource:///org/gnome/shell/ui/quickSettings.js';

export const DriveActivityIndicator = GObject.registerClass({
    GTypeName: 'DriveActivityIndicator',
}, class DriveActivityIndicator extends QuickSettings.SystemIndicator {
    constructor(extensionObject) {
        super();

        this._settings = extensionObject.getSettings();
        this._settingsValues = {
            includeRemovables: this._settings.get_boolean('include-removable-drives'),
            updateInterval: this._settings.get_int('update-interval'),
            idleColor: this._settings.get_string('idle-color'),
            readColor: this._settings.get_string('read-color'),
            writeColor: this._settings.get_string('write-color'),
            readWriteColor: this._settings.get_string('read-write-color'),            
        };
        this._settings.connectObject(
            'changed::include-removable-drives', () => {
                this._settingsValues.includeRemovables = this._settings.get_boolean('include-removable-drives');
            },
            'changed::update-interval', () => {
                this._settingsValues.updateInterval = this._settings.get_int('update-interval');
                this._create_loop();
            },
            'changed::idle-color', () => {
                this._settingsValues.idleColor = this._settings.get_string('idle-color');
            },
            'changed::read-color', () => {
                this._settingsValues.readColor = this._settings.get_string('read-color');
            },
            'changed::write-color', () => {
                this._settingsValues.writeColor = this._settings.get_string('write-color');
            },
            'changed::read-write-color', () => {
                this._settingsValues.readWriteColor = this._settings.get_string('read-write-color');
            },
            this
        );

        this._statFile = Gio.File.new_for_path('/proc/diskstats');
        this._reading = false; // Bussy flag
        this._cancellable = new Gio.Cancellable();
        this._lastDiskStats = "";

        this._activity = ""; // r = read, w = write, rw = read and write , other = no activty

        this._indicator = this._addIndicator();
        this._indicator.icon_name = 'drive-harddisk-symbolic';
        this._indicator.set_style(`color: ${this._settingsValues.idleColor}`);
        Main.panel.statusArea.quickSettings.addExternalIndicator(this);

        this._drives = [];

        this._volumeMonitor = Gio.VolumeMonitor.get();
        this._update_drives();

        this._volumeMonitor.connect('drive-connected', (_, drive) => {
            this._update_drives();
        });

        this._volumeMonitorId = this._volumeMonitor.connect('drive-disconnected', (_, drive) => {
            this._update_drives();
        });

        this._create_loop();

        this.connect('destroy', () => {
            this._settings.disconnectObject(this);
            this._settings = null;
            if (this._volumeMonitorId) {
                this._volumeMonitor.disconnect(this._volumeMonitorId);
                this._volumeMonitorId = null;
            }
            this._volumeMonitor = null;
            if (this._loopId) {
                GLib.Source.remove(this._loopId);
                this._loopId = null;
            }
            this._cancellable.cancel();
            this._cancellable = null;
            this._drives = null;
            this._statFile = null;
        });
    }

    _create_loop() {
        if (this._loopId) {
            GLib.Source.remove(this._loopId);
            this._loopId = null;
        }
        this._loopId = GLib.timeout_add(GLib.PRIORITY_DEFAULT, this._settingsValues.updateInterval, () => {
            if (!this._reading) {
                this._reading = true;
                this._statFile.load_contents_async(this._cancellable, (file, result) => {
                    try {
                        const [, contents, ] = file.load_contents_finish(result);
                        this._read_activity(contents);
                    } catch (e) {
                        if (!e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.CANCELLED)) {
                            console.error(e, `Reading ${file.get_path()}`);
                        }
                    }
                    this._reading = false;
                });
            } else {
                console.warn('Skip reading. Try increasing the interval.');
            }
            return GLib.SOURCE_CONTINUE;
        });
    }

    _read_activity(contents) {
        const decoder = new TextDecoder('utf-8');
        const currentDiskStats = decoder.decode(contents);
        if (currentDiskStats != this._lastDiskStats) {
            this._drives.forEach(drive => {
                if (!drive.is_removable() || this._settingsValues.includeRemovables) {
                    const identifier = drive.get_identifier(Gio.DRIVE_IDENTIFIER_KIND_UNIX_DEVICE).split('/').pop();
                    const regex = new RegExp('(' + identifier + ' .*)','g');
                    const currentData = currentDiskStats.match(regex)?.[0];
                    const lastData = this._lastDiskStats.match(regex)?.[0];
                    if (currentData != lastData) {
                        const currentFields = currentData.split(/[ ]+/g);
                        let activity = "";
                        if (lastData) {
                            const lastFields = lastData.split(/[ ]+/g);
                            if (currentFields[3] != lastFields[3]) {
                                activity += "r";
                            }
                            if (currentFields[7] != lastFields[7]) {
                                activity += "w";
                            }
                        }
                        this._update_icon(activity);
                    }
                }
            });
            this._lastDiskStats = currentDiskStats;
        } else {
            this._update_icon("")
        }
    }

    _update_drives() {
        this._drives = this._volumeMonitor.get_connected_drives();
        this._lastDiskStats = "";
    }

    _update_icon(newStatus) {
        if (newStatus != this._activity) {
            this._activity = newStatus;
            let color = this._settingsValues.idleColor;
            switch (this._activity) {
                case "r":
                    color = this._settingsValues.readColor;
                    break;
                case "w":
                    color = this._settingsValues.writeColor;
                    break;
                case "rw":
                    color = this._settingsValues.readWriteColor;
                    break;
                default:
                    color = this._settingsValues.idleColor;
                    break;
            }
            this._indicator.set_style(`color: ${color}`);
        }
    }
});