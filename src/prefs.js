'use strict';

import { ExtensionPreferences } from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js';

import { General } from './preferences/general.js';
import { About } from './preferences/about.js';

export default class Preferences extends ExtensionPreferences {
    fillPreferencesWindow(window) {
        window._settings = this.getSettings();
        window._metadata = this.metadata;
        
        window.add(new General(window));
        window.add(new About(window));
        
        window.search_enabled = true;
    }
}